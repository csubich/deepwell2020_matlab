function cbout = bwr(N)
% BWR: blue-white-red colormap

if (nargin < 1) N=128; end

topred = [0.5 0 0]; 
midred = [1 .3 .3]; 
white = [1 1 1]; 
midblue = [0.5 0.5 1]; 
botblue = [0 0 0.8];

scaledout = linspace(0,4,N);
cbout = zeros(N,3);
% Bottom blue to middle blue
cbout(scaledout<1,:) = interp1([0;1],[botblue;midblue],scaledout(scaledout<1));
% Middle blue to white
cbout(scaledout>=1 & scaledout < 2,:) = ...
    interp1([1;2],[midblue;white],scaledout(scaledout >= 1 & scaledout < 2));
% White to middle red
cbout(scaledout >= 2 & scaledout < 3,:) = ...
    interp1([2;3],[white;midred],scaledout(scaledout >=2 & scaledout < 3));
% Middle red to top red
cbout(scaledout >= 3 & scaledout <= 4,:) = ...
    interp1([3;4],[midred;topred],scaledout(scaledout >= 3 & scaledout <= 4));