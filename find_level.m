function eta = find_level(yg_yxz,rho_yxz,loc,rho_targ)
   % Finds the isopycnal level eta corresponding to target rho, when
   % it has been bracketed by loc
   
   % loc: vertical index (second domension) of the level just above the
   % transition
   
[Ny Nx Nz] = size(yg_yxz);
   

% Sparse matrix to 'select' the point above the crossover
sel_above = sparse(loc(:),1:(Nx*Nz),ones(Nx*Nz,1),Ny,Nx*Nz) > 0;
% And below
sel_below = sparse(max(1,-1+loc(:)),1:(Nx*Nz),ones(Nx*Nz,1),Ny,Nx*Nz) > 0;


% Get height and density at the points just above and below the transition
eta_above = yg_yxz(sel_above(:));
eta_below = yg_yxz(sel_below(:));
rho_above = rho_yxz(sel_above(:));
rho_below = rho_yxz(sel_below(:));

% Perform linear interpolation to find the approximate true height of the
% isosurface
eta_fin = eta_above.*(rho_targ - rho_below)./(rho_above - rho_below) + ...
          eta_below.*(rho_targ - rho_above)./(rho_below - rho_above);
eta_fin = reshape(eta_fin,[Nx,Nz]);

eta = eta_fin;