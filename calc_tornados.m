% Grids
xg = spins_reader('xgrid'); zg = spins_reader('zgrid'); yg = spins_reader('ygrid');
xxz = squeeze(xg(:,1,:)); zxz = squeeze(zg(:,1,:));

% Grid info
[Nx Ny Nz] = size(xg);
Lx = (max(xg(:))-min(xg(:)))*(Nx)/(Nx-1);
Ly = (max(yg(:))-min(yg(:)))*(Ny)/(Ny-1);
Lz = (max(zg(:))-min(zg(:)));

% Initial states
rho0 = spins_reader('rho',0);
u0 = spins_reader('u',0);

% Initial density profile info
maxrho = max(rho0(:)); minrho = min(rho0(:)); drho = maxrho - minrho;

% Quadrature weights
[dV, dxda, dxdc, dydb, dzda, dzdc] = get_jac(xg,yg,zg);
dxdz = sum(dV,2)/Ly;

% Boundary-layer mask
nu = 1e-6; % from config file
yplus = nu/max(abs(u0(:))); % Define a wall unit
bl_width = 2e2*yplus; % 200 wall-units
bl_dw = 0.5*bl_width; % Transition width

bl_mask = 0.5*(tanh((zg - zg(:,:,1) - bl_width)/(bl_dw)) + tanh((zg(:,:,end)-zg-bl_width)/(bl_dw)));
bl_mask_2d = mean(bl_mask,2);

% Length along z that is not boundary layer
interior_z = sum(dxdz.*bl_mask_2d,3)*Nx/Lx;

for kk=1:25

    % The idea here is that the positive and negative tornados leave
    % persistent, mode-1 perturbations on the background profile.  We can infer
    % their height from the depth-mean density, since if we lift the water
    % column up by epsilon then new fluid (of maximum density) must fill that
    % space, meanwhile we have displaced fluid of minimum density out of the
    % column.
    rho = spins_reader('rho',kk);
    rho2d = mean(rho,2); % Get depth-average density
    rho1d = sum(dxdz.*bl_mask_2d.*rho2d,3)./interior_z*Nx/Lx; % Get interior z-average

    % Rescale to a height
    dheight = (rho2d-rho1d)/drho*Ly;
    % Plot
    clf
    pcolor(xxz,zxz,squeeze(dheight.*bl_mask_2d)); shading flat; colorbar;
    
    % Find the max/min over the domain
    maxnado = max(dheight(:).*bl_mask_2d(:));
    minnado = min(dheight(:).*bl_mask_2d(:));
    caxis([-1 1]*max(maxnado,-minnado));
    axis([0.8 1.2 0 Lz]);
    hold on;
    % Plot boundaries
    fill(xxz(:,1),zxz(:,1),0.75*[1 1 1]);
    plot(xxz(:,1),zxz(:,1),'k-','linewidth',1.5);
    plot(xxz(:,end),zxz(:,end),'k-','linewidth',1.5);
    hold off
    title(sprintf('Heights at output %d',kk));
    drawnow; pause(1);
    
    fprintf('%3d: %.4e - %.4e\n',kk,minnado,maxnado)
    
end