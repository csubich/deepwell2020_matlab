% Track vortex by enstrophy centroids

% Boilerplate info for case
% Grids
xg = spins_reader('xgrid'); zg = spins_reader('zgrid'); yg = spins_reader('ygrid');
xxz = squeeze(xg(:,1,:)); zxz = squeeze(zg(:,1,:));

% Grid info
[Nx Ny Nz] = size(xg);
Lx = (max(xg(:))-min(xg(:)))*(Nx)/(Nx-1);
Ly = (max(yg(:))-min(yg(:)))*(Ny)/(Ny-1);
Lz = (max(zg(:))-min(zg(:)));

% Initial states
rho0 = spins_reader('rho',0);
u0 = spins_reader('u',0);

% Initial density profile info
%maxrho = max(rho0(:)); minrho = min(rho0(:)); drho = maxrho - minrho;
% Use analytic max/min densities
maxrho = 0.012;
minrho = -0.012;
drho = 0.024;

% Quadrature weights
[dV, dxda, dxdc, dydb, dzda, dzdc] = get_jac(xg,yg,zg);
dxdz = sum(dV,2)/Ly;

% Boundary-layer mask
nu = 1e-6; % from config file
yplus = nu/max(abs(u0(:))); % Define a wall unit
bl_width = 250*yplus; % 250 wall-units
bl_dw = 0.25*bl_width; % Transition width

% bl_mask = 0.5*(tanh((zg - zg(:,:,1) - bl_width)/(bl_dw)) + tanh((zg(:,:,end)-zg-bl_width)/(bl_dw)));
% Try BL mask that excludes the bump
bl_mask = 0.5*(tanh((zg - zg(1,1,1) - bl_width)/(bl_dw)) + tanh((zg(1,1,end)-zg-bl_width)/(bl_dw)));

bl_mask_2d = mean(bl_mask,2);

% Length along z that is not boundary layer
interior_z = sum(dxdz.*bl_mask_2d,3)*Nx/Lx;

for idx=0:25
    u = spins_reader('u',idx);
    v = spins_reader('v',idx);
    w = spins_reader('w',idx);
    rho = spins_reader('rho',idx);

    [ux uy uz] = get_grad(u);
    [vx vy vz] = get_grad(v);
    [wx wy wz] = get_grad(w);

    vort_x = wy - vz; % Streamwise-oriented vorticity
    vort_y = uz - wx; % Depth-oriented vorticity (Inherently 3D)
    vort_z = uy - vx; % Spanwise-oriented vorticity (2D vorticity)

    % Mask fluid above/below pycnocline based on current density
    rhomask = 0.5*(1+tanh(rho/(0.1*drho)));
    
    ens_y_mask = (bl_mask.*vort_y).^2;

    % 0th moment (total mass) of vort_y^2
    vysq_mom0_heavy = sum(rhomask(:).*ens_y_mask(:).*dV(:)); % Below pycnocline
    vysq_mom0_light = sum((1-rhomask(:)).*ens_y_mask(:).*dV(:)); % Above pycncline

    % Compute first moments (x,y,z)

    vysq_mom1x_heavy = sum(rhomask(:).*ens_y_mask(:).*dV(:).*xg(:)); % Below pycnocline
    vysq_mom1y_heavy = sum(rhomask(:).*ens_y_mask(:).*dV(:).*yg(:)); 
    vysq_mom1z_heavy = sum(rhomask(:).*ens_y_mask(:).*dV(:).*zg(:)); 

    vysq_mom1x_light = sum((1-rhomask(:)).*ens_y_mask(:).*dV(:).*xg(:)); % Below pycnocline
    vysq_mom1y_light = sum((1-rhomask(:)).*ens_y_mask(:).*dV(:).*yg(:)); 
    vysq_mom1z_light = sum((1-rhomask(:)).*ens_y_mask(:).*dV(:).*zg(:));

    % Normalized first moment (centroid / center of "mass")
    vysq_cent_heavy = [vysq_mom1x_heavy,vysq_mom1y_heavy,vysq_mom1z_heavy]/vysq_mom0_heavy;
    vysq_cent_light = [vysq_mom1x_light,vysq_mom1y_light,vysq_mom1z_light]/vysq_mom0_light;

    % Calculate filtered vorticities for enstrophy measure
    vort_x_med = median(vort_x,3);
    vort_y_med = median(vort_y,3);
    vort_z_med = median(vort_z,3);
    vort_x_filt = (vort_x - vort_x_med).*bl_mask;
    vort_y_filt = (vort_y - vort_y_med).*bl_mask;
    vort_z_filt = (vort_z - vort_z_med).*bl_mask;

    % Enstrophy based on filtered vorticities
    ens_filt = vort_x_filt.^2 + vort_y_filt.^2 + vort_z_filt.^2;
    ens_mom0_heavy = sum(rhomask(:).*ens_filt(:).*dV(:)); % Below pycnocline
    ens_mom0_light = sum((1-rhomask(:)).*ens_filt(:).*dV(:)); % Above

    ens_mom1x_heavy = sum(rhomask(:).*ens_filt(:).*dV(:).*xg(:));
    ens_mom1y_heavy = sum(rhomask(:).*ens_filt(:).*dV(:).*yg(:));
    ens_mom1z_heavy = sum(rhomask(:).*ens_filt(:).*dV(:).*zg(:));

    ens_mom1x_light = sum((1-rhomask(:)).*ens_filt(:).*dV(:).*xg(:));
    ens_mom1y_light = sum((1-rhomask(:)).*ens_filt(:).*dV(:).*yg(:));
    ens_mom1z_light = sum((1-rhomask(:)).*ens_filt(:).*dV(:).*zg(:));

    ens_cent_heavy = [ens_mom1x_heavy,ens_mom1y_heavy,ens_mom1z_heavy]/ens_mom0_heavy;
    ens_cent_light = [ens_mom1x_light,ens_mom1y_light,ens_mom1z_light]/ens_mom0_light;
    
    % Store results
    vysq_heavy_hist(idx+1) = vysq_mom0_heavy;
    vysq_light_hist(idx+1) = vysq_mom0_light;
    ens_heavy_hist(idx+1) = ens_mom0_heavy;
    ens_light_hist(idx+1) = ens_mom0_light;
    
    vysqc_heavy(:,idx+1) = vysq_cent_heavy;
    vysqc_light(:,idx+1) = vysq_cent_light;
    ensc_heavy(:,idx+1) = ens_cent_heavy;
    ensc_light(:,idx+1) = ens_cent_light;
    
    % Unfiltered vort_z^2, or approximately the enstropy attributable to
    % the wave motion
    vzsq_hist(idx+1) = sum(vort_z(:).^2.*dV(:)); 
    
    fprintf('%d complete\n',idx);
    fprintf('  heavy tornado (ens) %.3f %.3f %.3f\n',ens_cent_heavy);
    fprintf('              (v_y^2) %.3f %.3f %.3f\n',vysq_cent_heavy);
    fprintf('  light tornado (ens) %.3f %.3f %.3f\n',ens_cent_light);
    fprintf('              (v_y^2) %.3f %.3f %.3f\n',vysq_cent_light);
end

% Line plots of enstrophy evolution
orig_ens = vzsq_hist(1); % Original enstrophy in domain
figure(1); clf

subplot(2,1,1)
plot(0:25,100*vysq_light_hist/orig_ens,'b-',0:25,100*ens_light_hist/orig_ens,'r--')
title('3D enstrophy evolution above pycnocline')
ylabel('Percent')
legend('\omega_y^2/|\omega_0|^2','|\omega|^2/|\omega_0|^2','location','best')
subplot(2,1,2)
plot(0:25,100*vysq_heavy_hist/orig_ens,'b-',0:25,100*ens_heavy_hist/orig_ens,'r--')
ylabel('Percent')
xlabel('Time')
title('... Below pycnocline')

figure(2)
clf
plot(ensc_light(1,8:end),ensc_light(3,8:end),'bo-',...
     ensc_heavy(1,8:end),ensc_heavy(3,8:end),'r^-');
% Plot boundaries
hold on
fill(xxz(:,1),zxz(:,1),0.75*[1 1 1]);
plot(xxz(:,1),zxz(:,1),'k-','linewidth',1.5);
plot(xxz(:,end),zxz(:,end),'k-','linewidth',1.5);
axis([0.8 1.2 0 Lz]);

legend('Light tornado','Heavy tornado','location','best');
title('Vortex center (enstrophy measure) tracks');


figure(4)
clf
plot(vysqc_light(1,8:end),vysqc_light(3,8:end),'bo-',...
     vysqc_heavy(1,8:end),vysqc_heavy(3,8:end),'r^-');
% Plot boundaries
hold on
fill(xxz(:,1),zxz(:,1),0.75*[1 1 1]);
plot(xxz(:,1),zxz(:,1),'k-','linewidth',1.5);
plot(xxz(:,end),zxz(:,end),'k-','linewidth',1.5);
axis([0.8 1.2 0 Lz]);

legend('Light tornado','Heavy tornado','location','best');
title('Vortex center (\omega_y^2 measure) tracks');