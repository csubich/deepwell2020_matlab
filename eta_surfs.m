% Track vortices by isopycnal surfaces

% Boilerplate info for case
% Grids
xg = spins_reader('xgrid'); zg = spins_reader('zgrid'); yg = spins_reader('ygrid');
xxz = squeeze(xg(:,1,:)); zxz = squeeze(zg(:,1,:));

% Grid info
[Nx Ny Nz] = size(xg);
Lx = (max(xg(:))-min(xg(:)))*(Nx)/(Nx-1);
Ly = (max(yg(:))-min(yg(:)))*(Ny)/(Ny-1);
Lz = (max(zg(:))-min(zg(:)));

% Initial states
rho0 = spins_reader('rho',0);
u0 = spins_reader('u',0);

% Initial density profile info
%maxrho = max(rho0(:)); minrho = min(rho0(:)); drho = maxrho - minrho;
% Use analytic max/min densities
maxrho = max(rho0(:));
minrho = min(rho0(:));
medrho = 0;
% Set drho based on an idealized symmetric stratification
drho = max(maxrho,-minrho) - min(minrho,-maxrho);

% Quadrature weights
[dV, dxda, dxdc, dydb, dzda, dzdc] = get_jac(xg,yg,zg);
dxdz = sum(dV,2)/Ly;

% Boundary-layer mask
nu = 1e-6; % from config file
yplus = nu/max(abs(u0(:))); % Define a wall unit
bl_width = 250*yplus; % 250 wall-units
bl_dw = 0.25*bl_width; % Transition width

%bl_mask = 0.5*(tanh((zg - zg(:,:,1) - bl_width)/(bl_dw)) + tanh((zg(:,:,end)-zg-bl_width)/(bl_dw)));
% Try BL mask that excludes the bump
bl_mask = 0.5*(tanh((zg - zg(1,1,1) - bl_width)/(bl_dw)) + tanh((zg(1,1,end)-zg-bl_width)/(bl_dw)));

bl_mask_2d = mean(bl_mask,2);

% Isolevels to calculate statistics for
NLev = 5;
levs = medrho + drho * (-0.5 + (-0.5 + 1:NLev)/NLev)

lowmin = zeros(NLev,26); % Minimum value of filtered isosurface (counting from bottom)
highmin = zeros(NLev,26);% ... counting from top

lowmax = zeros(NLev,26); % Maximum value of filtered isosurface (counting from bottom)
highmax = zeros(NLev,26);% ... counting from top

% Centroids of absolute perturbation
lowcentx = zeros(NLev,26);
lowcentz = zeros(NLev,26);
highcentx = zeros(NLev,26);
highcentz = zeros(NLev,26);

% Overturning
overturn = zeros(NLev,26);

yg_yxz = permute(yg,[2 1 3]); % Permuted y-grid for height calculations

for idx=0:25

rho = spins_reader('rho',idx);
rho_yxz = permute(rho,[2 1 3]); % Permute for height-interpolation purposes

    for levidx = 1:NLev
        mylev = levs(levidx);

        % Get the "low" isosurface, where we count up from the bottom to find the
        % transition
        rho_lev = (rho < mylev);

        % Use 'max' to get the location of the maximum (first point, traversing
        % from the bottom)
        [val loc] = max(rho_lev,[],2);
        loc(val == 0) = Ny; % If the value isn't found in a water column, pick the top

        eta_low = find_level(yg_yxz, rho_yxz, loc, mylev);
        eta_low_median = median(eta_low,2);
        eta_low_filtered = squeeze(bl_mask_2d).*(eta_low - eta_low_median);

%         figure(1)
%         pcolor(xxz,zxz,eta_low_filtered); shading flat; colorbar; colormap bwr
%         caxis([-1 1]*max(abs(eta_low_filtered(:))));
%         axis([0.8 1.2 0 Lz]);

        % Get the "high" isosurface, where we count down from the top

        % The signal array is now transitioning from below-level to above-level
        rho_lev = (rho > mylev);
        % And flip the density array for the maximum
        [val loc] = max(flip(rho_lev,2),[],2);

        % Indices now need adjustment
        loc = min(Ny,2+Ny-loc);
        loc(val == 0) = Ny; % If the value isn't found in a water column, pick the top

        eta_high = find_level(yg_yxz, rho_yxz, loc, mylev);
        eta_high_median = median(eta_high,2);
        eta_high_filtered = squeeze(bl_mask_2d).*(eta_high - eta_high_median);
% 
%         figure(2)
%         pcolor(xxz,zxz,eta_high_filtered); shading flat; colorbar; colormap bwr
%         caxis([-1 1]*max(abs(eta_high_filtered(:))));
%         axis([0.8 1.2 0 Lz]);

        lowmin(levidx,idx+1) = min(eta_low_filtered(:));
        highmin(levidx,idx+1) = min(eta_high_filtered(:));
        lowmax(levidx,idx+1) = max(eta_low_filtered(:));
        highmax(levidx,idx+1) = max(eta_high_filtered(:));
        
        overturn(levidx,idx+1) = max(eta_high_filtered(:)-eta_low_filtered(:));

        lowcentx(levidx,idx+1) = sum(dxdz(:).*(eta_low_filtered(:).^2).*xxz(:))./...
                                 sum(dxdz(:).*(eta_low_filtered(:).^2));
        lowcentz(levidx,idx+1) = sum(dxdz(:).*(eta_low_filtered(:).^2).*zxz(:))./...
                                 sum(dxdz(:).*(eta_low_filtered(:).^2));
        highcentx(levidx,idx+1) = sum(dxdz(:).*(eta_high_filtered(:).^2).*xxz(:))./...
                                  sum(dxdz(:).*(eta_high_filtered(:).^2));
        highcentz(levidx,idx+1) = sum(dxdz(:).*(eta_high_filtered(:).^2).*zxz(:))./...
                                  sum(dxdz(:).*(eta_high_filtered(:).^2));

    end
end

% Label array for plotting
for lidx = 1:NLev
    label{lidx} = sprintf('\\rho = %.2e',levs(lidx));
end
% 
% Isopycnal depression
figure(1)
plot(0:25,lowmin);
title('Extreme vortex-induced depression of isopycnals')
ylabel('\Delta z')
xlabel('t');
legend(label,'location','best')


% Isopycnal elevation
figure(2)
plot(0:25,highmax);
title('Extreme vortex-induced elevation of isopycnals')
ylabel('\Delta z')
xlabel('t');
legend(label,'location','best')

% Centroids
figure(3)
clf
plts = plot(highcentx([1 5],2:end)',highcentz([1 5],2:end)','.-')

% Plot boundaries
hold on
fill(xxz(:,1),zxz(:,1),0.75*[1 1 1]);
plot(xxz(:,1),zxz(:,1),'k-','linewidth',1.5);
plot(xxz(:,end),zxz(:,end),'k-','linewidth',1.5);
axis([0.8 1.2 0 Lz]);
% Legend
legend(plts,sprintf('\\rho = %+.2e',levs(1)), ...
            sprintf('\\rho = %+.2e',levs(5)), 'location', 'best')
title('"Tornado" tracks')
% text(highcentx(1,2),highcentz(1,2),'t=1')
text(highcentx(1,26),highcentz(1,26),'t=26')

text(highcentx(5,2),highcentz(5,2),'t=1')
% text(highcentx(5,26),highcentz(5,26),'t=26')