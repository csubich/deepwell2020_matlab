% Get and plot moments of density

% Get grid and rho field
xg = spins_reader('xgrid'); zg = spins_reader('zgrid'); yg = spins_reader('ygrid');
rho = spins_reader('rho',8);

[Nx Ny Nz] = size(rho);
Lx = (max(xg(:))-min(xg(:)))*(Nx)/(Nx-1);
Ly = (max(yg(:))-min(yg(:)))*(Ny)/(Ny-1);
Lz = (max(zg(:))-min(zg(:)));

% Get 2D grid slices
xxz = squeeze(xg(:,1,:)); zxz = squeeze(zg(:,1,:));

% Get gradient.  MAKE SURE get_grad is the modified version which uses
% dct3_dy, not fft_dy.
[rhox, rhoy, rhoz] = get_grad(rho);

% Zeroth moment of rho_y: top to bottom density difference (unit rho)
mom0 = sum(rhoy,2)*Ly/Ny;

% Scaled first moment: buoyancy centroid (unit L)
mom1 = sum(rhoy.*yg,2)*Ly/Ny./mom0;

% Second moment: standard deviation of buoyancy (measure of pycnocline
% width), unit L
mom2 = sqrt(sum(rhoy.*(yg-mom1).^2,2)*Ly/Ny./mom0);

% Third moment: moment coefficient of skewness -- unit L
mom3 = sum(rhoy.*(yg-mom1).^3,2)*Ly/Ny./mom0./mom2.^2;

figure(1) % Raw moments

subplot(2,2,1); 
pcolor(xxz,zxz,squeeze(mom0)); shading flat; axis([0.8 1.2 0 0.06]);
colorbar
title('0th moment (mass)')
subplot(2,2,2)
pcolor(xxz,zxz,squeeze(mom1)); shading flat; axis([0.8 1.2 0 0.06]);
colorbar
title('1st moment (centroid)');
subplot(2,2,3);
pcolor(xxz,zxz,squeeze(mom2)); shading flat; axis([0.8 1.2 0 0.06]);
colorbar
title('2nd moment (std)');
subplot(2,2,4);
pcolor(xxz,zxz,squeeze(mom3)); shading flat; axis([0.8 1.2 0 0.06]);
colorbar
title('3rd moment (skewness)');
colormap bwr

% Filter out waves

% Calculate boundary-layer mask

% Quadrature weights
[dV, dxda, dxdc, dydb, dzda, dzdc] = get_jac(xg,yg,zg);
dxdz = sum(dV,2)/Ly;

% Initial u-field to define boundary layer
u0 = spins_reader('u',0);

nu = 1e-6; % from config file
yplus = nu/max(abs(u0(:))); % Define a wall unit
bl_width = 2e2*yplus; % 200 wall-units
bl_dw = 0.5*bl_width; % Transition width

% 3D mask
bl_mask = 0.5*(tanh((zg - zg(:,:,1) - bl_width)/(bl_dw)) + tanh((zg(:,:,end)-zg-bl_width)/(bl_dw)));
% 2D version
bl_mask_2d = mean(bl_mask,2);

% Length along z that is not boundary layer, for normalization
interior_z = sum(dxdz.*bl_mask_2d,3)*Nx/Lx;

% Filter moments
% mom0_filt = mom0 - sum(bl_mask_2d.*mom0.*dxdz,3)./interior_z*Nx/Lx;
% mom1_filt = mom1 - sum(bl_mask_2d.*mom1.*dxdz,3)./interior_z*Nx/Lx;
% mom2_filt = mom2 - sum(bl_mask_2d.*mom2.*dxdz,3)./interior_z*Nx/Lx;
% mom3_filt = mom3 - sum(bl_mask_2d.*mom3.*dxdz,3)./interior_z*Nx/Lx;


mom0_filt = mom0 - median(mom0,3);
mom1_filt = mom1 - median(mom1,3);
mom2_filt = mom2 - median(mom2,3);
mom3_filt = mom3 - median(mom3,3);


figure(2)
subplot(2,2,1);
pcolor(xxz,zxz,squeeze(mom0_filt)); shading flat; colorbar; axis([0.8 1.2 0 0.06]);
caxis([-1 1]*max(abs(mom0_filt(:))));
title('Filtered 0th moment')
subplot(2,2,2);
pcolor(xxz,zxz,squeeze(mom1_filt)); shading flat; colorbar; axis([0.8 1.2 0 0.06]);
caxis([-1 1]*max(abs(mom1_filt(:))));
title('Filtered 1st moment')
subplot(2,2,3);
pcolor(xxz,zxz,squeeze(mom2_filt)); shading flat; colorbar; axis([0.8 1.2 0 0.06]);
caxis([-1 1]*max(abs(mom2_filt(:))));
title('Filtered 2nd moment')
subplot(2,2,4);
pcolor(xxz,zxz,squeeze(mom3_filt)); shading flat; colorbar; axis([0.8 1.2 0 0.06]);
caxis([-1 1]*max(abs(mom3_filt(:))));
title('Filtered 3rd moment')
colormap bwr

figure(3)
% Select slice to plot
ii=276;yyz = squeeze(yg(ii,:,:)); zyz = squeeze(zg(ii,:,:));
subplot(2,1,1)
pcolor(zyz,yyz,squeeze(rho(ii,:,:))); shading flat; colormap(flipud(bwr))
caxis([-1 1]*1e-2)
axis([0 0.1 0.08 0.15])
title('Density slice, t=8, x=1.076')
xlabel('y'); ylabel('z')
xlabel('')
subplot(2,1,2)
plot(squeeze(zg(ii,1,:)),mom1_filt(ii,:),squeeze(zg(ii,1,:)),mom2(ii,:),squeeze(zg(ii,1,:)),mom3(ii,:))
legend('filtered centroid','std','skew')
title('Moments')
xlabel('x')