function [dV, dxda, dxdc, dydb, dzda, dzdc] = get_jac(xg,yg,zg)

[Nx Ny Nz] = size(xg);
Lx = (max(xg(:))-min(xg(:)))*Nx/(Nx-1);
Ly = (max(yg(:))-min(yg(:)))*Ny/(Ny-1);
Lz = max(zg(:))-min(zg(:));

Pz = sign(zg(1,1,1)-zg(1,1,end));
Px = sign(xg(end,1,1)-xg(1,1,1));
Py = sign(yg(1,end,1)-yg(1,1,1));

Cgx = Px*Lx/Nx*(1:Nx)';

dxda = fft3_dx(xg-Cgx) + Px*Lx/2/pi;
dxdc = cvdd(xg,3);

dzda= fft3_dx(zg);
dzdc = cvdd(zg,3);

dydb = Ly/pi*Py;

quad_a = repmat(2*pi/Nx,Nx,1,1);
quad_b = repmat(pi/Ny,1,Ny,1);
[~,quad_c] = clencurt(Nz-1);
quad_c = reshape(Pz*quad_c,1,1,Nz);

dV = (quad_a.*quad_b.*quad_c).*(dxda.*dydb.*dzdc - dxdc.*dydb.*dzdc);